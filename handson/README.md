# Starter Kit ハンズオン

Container Stater Kitで実施するハンズオンの手順を紹介します。

![overview](./img/handson-overview.png)
## 0. 環境への接続
---
インストラクターから提示されたOpenShiftクラスタの Web コンソールの URL にアクセスし、自身のユーザーでログインして下さい。  

このハンズオンでは以下を使用してパイプラインの動作を確認していきます。
- 作業環境としての Web IDE : Red Hat OpenShift Dev Spaces
- OpenShift API リソースの作成 : OpenShift CLI ( `oc` )
- パイプラインリソースの確認、情報取得 : Tekton CLI ( `tkn` )
- パイプラインの実行状況確認 : OpenShift Web コンソール

ログイン後、以下の手順で Red Hat OpenShift Dev Spaces (RHOSDS) Workspace を起動してください。

- 右上の Application Menu のアイコンから RHOSDS にアクセス。
![application-menu](./img/application-menu.png)

- OpenShift のユーザで RHOSDS にログイン。 `Authorize Access` 画面が表示されたら `Allow selected permissions` を選択。
![login](./img/openshift-login.png)

- `Create Workspace` を選択し、 `Git Repo URL` に devfile の URL を入力。 `Create & Open` を選択。
![rhosds-create-workspace](./img/rhosds-create-workspace.png)

- RHOSDS Workspace が起動することを確認。
![rhosds-workspace](./img/rhosds-workspace.png)

- 画面右側の ![rhosds-container-icon](./img/rhosds-container-icon.png) アイコンを選択し、 `wto` > `New terminal` > `starter-kit-cicd-manifest` を選択してターミナルを起動。
![rhosds-terminal](./img/rhosds-terminal.png)

## 1. パイプラインの作成
---
![pipelines](./img/pipelines-cr.png)

### 1-1. 実行環境の準備
---
パイプラインを実行するために必要な設定等を行います。
```
# 環境変数の設定
export USER=$(oc whoami)

# 作業を行うProjectの作成
oc new-project ci-${USER}
```

### 1-2. Taskの作成
---
Pipelineの作成に必要なTaskを自身のProjectにデプロイします。
```
cd /projects/starter-kit-cicd-manifest/handson/tasks/
oc apply -f .

# 確認
tkn task list
---
NAME                DESCRIPTION              AGE
git-clone           These Tasks are Git...   17 minutes ago
sonarqube-scanner   The following task ...   17 minutes ago
trivy               This Task scans Com...   17 minutes ago
```
通常のTaskはNamespace-scopedなカスタムリソースですが、クラスタ全体で利用可能なClusterTaskというカスタムリソースも存在します。  
OpenShiftではOpenShift Pipelines Operatorをインストールすると、デフォルトで複数のClusterTaskが作成されすぐに利用することができます。

```
tkn clustertask list
---
NAME                        DESCRIPTION              AGE
argocd-task-sync-and-wait   This task syncs (de...   1 day ago
buildah                     Buildah task builds...   1 day ago
buildah-1-7-0               Buildah task builds...   1 day ago
git-cli                     This task can be us...   1 day ago
git-clone                   These Tasks are Git...   1 day ago
...
```

### 1-3. Pipelineの作成
---
インストールしたTaskを使いPipelineを作成します。  
handson/pipelinesディレクトリにあるhandson-pipeline.yamlファイルをエディタで修正していきましょう。

```
# エディタで編集
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: handson-pipeline
spec:
...
  # XXXXXXXXXXXとなっている部分に値を入力していく
  tasks:
  - name: git-clone-health
    taskRef:
      name: XXXXXXXXXXX
    params:
    - name: XXXXXXXXXXX
      value: $(params.git-url)
    - name: XXXXXXXXXXX
      value: $(params.git-revision)
    workspaces:
    - name: XXXXXXXXXXX
      workspace: shared-workspace
...
```

#### **Pipeline作成のヒント**
1-2でインストールしたそれぞれのTaskの中身を確認し、実行に必要なパラメーターやWorkspacesについて確認しましょう。
![tips](./img/pipeline-tips.png)
また、この READMEの冒頭にあるハンズオン概要図から実行すべきTaskの順番を確認しましょう。

進めるのが難しいと感じたら/handson/answers以下の回答を確認してください。

各 Task で利用可能なパラメータや workspace については以下のように確認できます。

```
tkn task describe git-clone
---
Name:          git-clone
Namespace:     ci-user1
Description:   These Tasks are Git tasks to work with repositories used by other tasks in your Pipeline.
The git-clone Task will clone a repo from the provided url into the output Workspace. By default the repo will be cloned into the root of your Workspace. You can clone into a subdirectory by setting this Task's subdirectory param. This Task also supports sparse checkouts. To perform a sparse checkout, pass a list of comma separated directory patterns to this Task's sparseCheckoutDirectories param.
...
⚓ Params

 NAME                          TYPE     DESCRIPTION              DEFAULT VALUE
 ∙ url                         string   git url to clone         ---
 ∙ revision                    string   git revision to che...   
 ∙ refspec                     string   (optional) git refs...   
 ∙ submodules                  string   defines if the reso...   true
 ∙ depth                       string   performs a shallow ...   1
 ∙ sslVerify                   string   defines if http.ssl...   true
 ∙ subdirectory                string   subdirectory inside...   
 ∙ sparseCheckoutDirectories   string   defines which direc...   
 ∙ deleteExisting              string   clean out the conte...   true
 ∙ httpProxy                   string   git HTTP proxy serv...   
 ∙ httpsProxy                  string   git HTTPS proxy ser...   
 ∙ noProxy                     string   git no proxy - opt ...   
 ∙ verbose                     string   log the commands us...   true
 ∙ gitInitImage                string   the image used wher...   gcr.io/tekton-releases/github.com/tektoncd/pipeline/cmd/git-init:v0.21.0

📝 Results

 NAME       DESCRIPTION
 ∙ commit   The precise commit ...
 ∙ url      The precise URL tha...

📂 Workspaces

 NAME       DESCRIPTION
 ∙ output   The git repo will b...

🦶 Steps

 ∙ clone
...
```

handson-pipeline.yamlを作成できたらクラスタにデプロイします。
```
cd /projects/starter-kit-cicd-manifest/handson/pipelines/
cat handson-pipeline.yaml | envsubst |oc apply -f -

# 確認 
tkn pipeline describe handson-pipeline 
---
Name:        handson-pipeline
Namespace:   ci-user1

📦 Resources

 No resources

⚓ Params

 NAME             TYPE     DESCRIPTION   DEFAULT VALUE
 ∙ git-url        string                 ---
 ∙ git-revision   string                 ---
 ∙ target-path    string                 ---

📝 Results

 No results

📂 Workspaces

 NAME                 DESCRIPTION
 ∙ shared-workspace   Git Repo for Patien...
 ∙ sonar-settings     Temporary dir for s...
 ∙ vul-cache          Cache vulnerabiliti...

🗒  Tasks

 NAME                 TASKREF             RUNAFTER           TIMEOUT   CONDITIONS   PARAMS
 ∙ git-clone-health   git-clone                              ---       ---          url: string, revision: string
 ∙ scan-app           sonarqube-scanner   git-clone-health   ---       ---          SONAR_HOST_URL: http://sonarqube.user1-develop.svc.cluster.local:9000, SONAR_PROJECT_KEY: ci-user1
 ∙ build-container    s2i-nodejs          scan-app           ---       ---          PATH_CONTEXT: , IMAGE: 
 ∙ test-container     trivy               build-container    ---       ---          IMAGE: image-registry.openshift-image-registry.svc:5000/ci-user1/health-record, IMAGE_DIGEST: 

⛩  PipelineRuns

 No pipelineruns
```

### 1-4. PipelineRunの作成
---
1-3で作成したPipelineが正しく動作するかPipelineRunを作成して確認を行います。
handson/pipelinesディレクトリにあるhandson-pipelinerun.yamlファイルを先ほどと同様にエディタで修正していきます。

こちらも/handson/answers以下に回答を用意しています。 (指定する URL は適宜現在の環境のものに変更してください)

pipeline で利用可能なパラメータや workspace については以下のように確認できます。

```
tkn pipeline describe handson-pipeline 
---
Name:        handson-pipeline
Namespace:   ci-user1

📦 Resources

 No resources

⚓ Params

 NAME             TYPE     DESCRIPTION   DEFAULT VALUE
 ∙ git-url        string                 ---
 ∙ git-revision   string                 ---
 ∙ target-path    string                 ---

📝 Results

 No results

📂 Workspaces

 NAME                 DESCRIPTION
 ∙ shared-workspace   Git Repo for Patien...
 ∙ sonar-settings     Temporary dir for s...
 ∙ vul-cache          Cache vulnerabiliti...

🗒  Tasks

 NAME                 TASKREF             RUNAFTER           TIMEOUT   CONDITIONS   PARAMS
 ∙ git-clone-health   git-clone                              ---       ---          url: string, revision: string
 ∙ scan-app           sonarqube-scanner   git-clone-health   ---       ---          SONAR_HOST_URL: http://sonarqube.user1-develop.svc.cluster.local:9000, SONAR_PROJECT_KEY: ci-user1
 ∙ build-container    s2i-nodejs          scan-app           ---       ---          PATH_CONTEXT: , IMAGE: 
 ∙ test-container     trivy               build-container    ---       ---          IMAGE: image-registry.openshift-image-registry.svc:5000/ci-user1/health-record, IMAGE_DIGEST: 

⛩  PipelineRuns

 No pipelineruns
```

handson-pipelinerun.yamlを作成できたらクラスタにデプロイしていきましょう。

Workspaceとして使用するPVを作成し、その後PipelineRunを実行します。
```
oc apply -f tekton-pvc.yaml

# PipelineRunのマニフェストを実行する場合、最後のコマンドが"oc create -f -"となるため注意
oc create -f handson-pipelinerun.yaml
```

これで先ほど作成したPipelineが実行されました。  
OpenShiftのコンソールにログインし、自身のProjectでパイプラインが実行されていることを確認しましょう。
画面左側のパイプラインからパイプライン実行を選択すると実行状況を確認することができます。  
  
  
![tips](./img/pipeline-success.png)

無事にパイプラインが成功していることを確認できました。  
もし実行失敗となっている場合、ログやイベントから失敗原因を確認し、yamlファイルの修正を行った上であらためて実行してみましょう。  
  
## 2. トリガーの作成
---
ここからは作成したPipelineをGitリポジトリからのWebhookを受けて実行するため、
Tekton Triggersのカスタムリソースオブジェクトを作成していきます。
![triggers](./img/triggers-cr.png)

### 2-1. TriggerTemplateの作成
まず初めにTriggerTemplateを作成します。handson/triggersディレクトリにあるhandson-template.yamlをエディタで開き編集します。  
TriggerTemplateの中身はPipelineRunとほぼ同じですが、Webhookで受け取った値をパラメーターとして利用するための設定が追加されています。

編集が完了したらhandson/triggersディレクトリに移動し、クラスタに対しデプロイします。
```
cd /projects/starter-kit-cicd-manifest/handson/triggers
oc apply -f handson-template.yaml

# 確認
tkn triggertemplate list
---
NAME               AGE
handson-template   20 seconds ago
```

### 2-2. TriggerBindingの作成
続いてTriggerBindingを作成していきます。
こちらは編集箇所が無いためhandson/triggersディレクトリにあるhandson-binding.yamlファイルの中身を確認し、そのままデプロイしましょう。
```
oc apply -f handson-binding.yaml

# 確認
tkn triggerbinding list
---
NAME              AGE
handson-binding   12 seconds ago
```
以下のGiteaのページを合わせて確認し、Webhookのどのパラメーターを取得しているか確認してみて下さい。  
https://docs.gitea.io/en-us/webhooks/

### 2-3. SecretとServiceAccountの作成
EventListenerを作成する前に、必要なSecretとServiceAccountを作成していきます。
まずWebhookのSecret keyとなるSecretを作成します。
```
# 任意の値を設定して下さい
export SECRET_TOKEN=Webhook用トークン
oc create secret generic git-webhook --from-literal=secretkey=${SECRET_TOKEN}
```

続いてServiceAccountを作成します。  
sample-sa.yamlではPipelineRun作成に必要なRBAC設定についても併せて実施しています。
```
cat sample-sa.yaml | envsubst |oc apply -f -
```  



### 2-4. EventListenerの作成
最後にEventListenerを作成します。
handson-listener.yamlファイルを修正していきましょう。

編集が完了したらクラスタに対しデプロイします。
```
oc apply -f handson-listener.yaml

# 確認
tkn eventlistener list
---
NAME               AGE              URL                                                          AVAILABLE
handson-listener   14 seconds ago   http://el-handson-listener.ci-user1.svc.cluster.local:8080   True
```

EventListenerはデフォルトでは外部公開されていないため、Routeを作成します。
後ほどこのURLをWebhookの宛先として設定します。
```
# Routeの作成
oc expose service el-handson-listener

# 確認
oc get route el-handson-listener
NAME                  HOST/PORT                                   PATH   SERVICES              PORT            TERMINATION   WILDCARD
el-handson-listener   el-handson-listener-xxx.openshiftapps.com          el-handson-listener   http-listener                 None

# URL の確認
echo http://$(oc get route el-handson-listener -o jsonpath='{.spec.host}')
```
試しにブラウザからアクセスしてみましょう。（適宜URLを置き換えて下さい）  
http://el-handson-listener-xxx.openshiftapps.com

以下のような表示がされていればEventListenerが正しく動作しています。  
`{"eventListener":"handson-listener","namespace":"ci-<ユーザ名>","eventListenerUID":"","errorMessage":"Invalid event body format format: unexpected end of JSON input"}`

## 3. CIの実行
### 3-1. GitリポジトリでのWebhook設定
2-4で作成したEventListenerに対しWebhookを実行するようGitリポジトリの設定を行なっていきます。
ブラウザからForkしたアプリケーションリポジトリを開きましょう。

http://gitea-<ユーザ名>-develop.apps.cluster-82dcv.82dcv.sandbox784.opentlc.com/gitea/starter-kit-cicd-app

画面右上の`サインイン`からユーザ名、パスワードを入力してサインインします。ユーザ名は gitea、パスワードは openshift です。

画面右上の`Settings -> Webhook -> Add Webhook -> Gitea`からWebhookの設定を行います。
各入力項目に以下を設定しましょう。
```
Target URL: 2-4で作成したEventListenerのURL
Secret: 2-3で入力したトークンの値 
Push events: チェックを入れ、Branch filter に"main"と入力
```
設定が出来たら`Add webhook`ボタンをクリックします。
作成したWebhookにて、`Test Delivery`を実行すると、パイプラインが実行されます。
OpenShiftコンソールを開き、新たなパイプライン実行が作成されていることを確認してみましょう。

### 3-2. 開発の流れの中でのCI実行

ここまででCIを実行するために必要な設定を全て実施できました。  
最後に開発の流れの中でCIパイプラインがどのように実行されるか確認しておきましょう。

![githubflow](./img/githubflow.png)
今アプリケーションリポジトリはGitHub Flowで開発が行われ、mainとfeatureという2つのブランチが存在しています。  
開発者はfeatureで新機能の開発を行い、それをmainにマージしていきます。  

あらためてブラウザでアプリケーションリポジトリを開き、**featureブランチ**を選択しましょう。
![feature](./img/feature-branch.png)


/site/public/index.html を選択し
画面右上のペンのマークから編集画面を開き、74行目以下のコメントアウトを外します。
```
# before
      <!--
      <div class="box">
        <div class="map" id='map'></div>
      </div>
      -->

# after
      <div class="box">
        <div class="map" id='map'></div>
      </div>
```
変更できたらCommitを実施しましょう。  
![commit](./img/commit.png)  

画面上の`Pull Requests -> New Pull Request`から新しいプルリクエストを作成します。
`pull from`の部分を`feature`に変更し`New Pull Request -> Create Pull Request`でプルリクエストを作成します。

![mr](./img/pullrequest.png)   
続けてマージリクエスト画面で`Merge Pull Request`ボタンを押しましょう。  
これでfeatureで追加した変更がmainに取り込まれるとともに、CIパイプラインが実行されました。  
OpenShiftコンソールからパイプラインの実行を確認しましょう。

以上で本ハンズオンは終了です。お疲れ様でした。


